from typing import Callable


def integral(f: Callable[[float], float], a: float, b: float, n: int):
    h: float = (b - a) / n
    xl: float = a
    xr: float = xl + h

    result: float = 0.0

    for i in range(n):
        result += (f(xl) + f(xr)) * h / 2
        xl = xr
        xr = xl + h

    return result


def main() -> int:
    print("Enter a single argument Python lambda declaration")
    f: Callable[[float], float] = eval(input())

    print("Enter left end of the interval (float):")
    a: float = float(input())

    print("Enter right end of the interval (float):")
    b: float = float(input())

    print("Enter n (int):")
    n: int = int(input())

    print("Integral:", integral(f, a, b, n))

    return 0


if __name__ == "__main__":
    exit(main())
